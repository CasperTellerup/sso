# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2015-12-12 20:46
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('organisations', '0005_auto_20151212_2143'),
    ]

    operations = [
        migrations.RunSQL(
            ["UPDATE organisations_organisationaddress SET address_type='physical' WHERE address_type='meditation';"
             "UPDATE organisations_organisationaddress SET address_type='postal' WHERE address_type='post';"],
            ["UPDATE organisations_organisationaddress SET address_type='meditation' WHERE address_type='physical';"
             "UPDATE organisations_organisationaddress SET address_type='post' WHERE address_type='postal';"],
        )
    ]
